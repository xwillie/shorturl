CREATE TABLE IF NOT EXISTS urls (
  "shorturl" varchar(20) NOT NULL,
  "longurl" varchar(255) NOT NULL
)
;