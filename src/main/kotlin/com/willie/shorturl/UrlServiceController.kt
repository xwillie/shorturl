package com.willie.shorturl

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import com.willie.shorturl.UrlModel

@RestController
class UrlServiceController {
    val rootUrl = "http://sho.rt/"
    @PostMapping("/generate/shortURL")
    fun shortUrl(@RequestBody url:UrlModel, urlModel: UrlModel): String {
        url.shortUrl = generateRandom(7)
        urlModel.save(url)
        return rootUrl + url.shortUrl
    }

    @GetMapping("/generate/longURL")
    fun longUrl(shortUrl: String): String {
        return "The long URL"
    }

    fun generateRandom(length: Int): String{
        val chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toList().toTypedArray()
        return (1..length).map { chars.random() }.joinToString("")
    }
}