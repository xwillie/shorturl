package com.willie.shorturl

import java.util.Properties
import org.jooq.*
import org.jooq.impl.*
import org.jooq.impl.DSL.*

import com.willie.jooq.tables.*

class UrlModel {
    var shortUrl: String = ""
    var longUrl: String = ""

    fun getURLByShortURLKey(shortURLKey: String): String {
        val properties = Properties()
//        properties.load(properties::class.java.getResourceAsStream("/application.properties"))

        DSL.using(
                "jdbc:postgresql://localhost:5433/shorturldb",
                "user",
                "pass123"
        ).use {
            ctx ->
            val url = Urls.URLS

            ctx.select(url.LONGURL, url.SHORTURL)
                    .from(url)
                    .where(url.SHORTURL.eq(shortURLKey))
                    .forEach {
                        println("${it[url.LONGURL]}")
                    }
        }
        return ""
    }

    fun save(urlObject: UrlModel) {
        val properties = Properties()
//        properties.load(properties::class.java.getResourceAsStream("/application.properties"))

//        DSL.using(
//                "jdbc:postgresql://localhost:5433/shorturldb",
//                "user",
//                "pass123"
//        ).use {
//            ctx ->
//            val url = Urls.URLS
//            ctx.insertInto(url)
//                    .set(urlObject)
//                    .execute()
//        }
    }
}