import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.rohanprabhu.gradle.plugins.kdjooq.*

plugins {
	id("org.springframework.boot") version "2.2.4.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	id("com.rohanprabhu.kotlin-dsl-jooq") version "0.4.4"
	id ("org.flywaydb.flyway") version "6.2.2"
	kotlin("jvm") version "1.3.61"
	kotlin("plugin.spring") version "1.3.61"
}

group = "com.willie"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val developmentOnly by configurations.creating
configurations {
	runtimeClasspath {
		extendsFrom(developmentOnly)
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-jooq")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.postgresql:postgresql")
	implementation("org.flywaydb:flyway-core")
	jooqGeneratorRuntime("org.postgresql:postgresql")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}

jooqGenerator {
	configuration("primary",  sourceSets.getByName("main")) {
		configuration = jooqCodegenConfiguration {
			jdbc {
				username = "user"
				password = "pass123"
				driver   = "org.postgresql.Driver"
				url      = "jdbc:postgresql://localhost:5433/shorturldb"
			}

			generator {
				target {
					packageName = "com.willie.jooq"
					directory   = "${project.projectDir}/src/main/kotlin/com/willie/generated"
				}

				database {
					name = "org.jooq.meta.postgres.PostgresDatabase"
					inputSchema = "public"
				}
			}
		}
	}
}

flyway {
    url = "jdbc:postgresql://localhost:5433/shorturldb"
    user = "user"
    password = "pass123"
	baselineOnMigrate = true
//    schemas = ["public"]
}