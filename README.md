# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Resources ###
* https://github.com/jOOQ/jOOQ/blob/master/jOOQ-examples/jOOQ-kotlin-example/src/main/kotlin/org/jooq/example/kotlin/FunWithKotlinAndJOOQ.kt
* https://flywaydb.org/documentation/gradle/
* https://github.com/bastman/spring-kotlin-jooq

### How do I get set up? ###

* Use postgres from this repo: `git clone git@bitbucket.org:xwillie/postgres_docker.git`
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### To do ###
* Finish on the functionality
* Dockerize
* Implement Caching using redis
* Add Logging
* Use JUNIT5 for tests
* Slap a UI on it

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact